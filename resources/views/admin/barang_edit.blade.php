@extends('layout.main')

@section('content')
    <div class="row mt-4">
        <div class="col-12 bg-white shadow-sm p-3">
            <div class="w-100 d-flex justify-content-between align-items-center mb-5">
                <h4 class="fs-3 py-2 px-3 border-bottom border-dark border-3 d-inline">Edit Barang</h4>
                <div class="kanan">
                    <a href="{{ route('data.barang') }}" class="btn btn-secondary">Kembali</a>
                </div>
            </div>


            <form action="{{ route('barang.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <input type="hidden" name="id" value="{{ $brg->id }}">
                <input type="hidden" name="photo_lama" value="{{ $brg->foto }}">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Nama</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="nama" name="nama" value="{{ $brg->nama }}">
                                @error('nama')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Letak</p>
                            </div>
                            <div class="col-md-7">
                                <select class="form-select" name="letak">
                                    @foreach ($letak as $lt)
                                        @if ($brg->letak == $lt)
                                            <option value="{{ $lt }}" selected>{{ $lt }}</option>
                                        @else
                                            <option value="{{ $lt }}">{{ $lt }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('letak')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Kondisi</p>
                            </div>
                            <div class="col-md-7">
                                <select class="form-select" name="kondisi">
                                    @foreach ($kondisi as $kn)
                                        @if ($brg->kondisi == $kn)
                                            <option value="{{ $kn }}" selected>{{ $kn }}</option>
                                        @else
                                            <option value="{{ $kn }}">{{ $kn }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('kondisi')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Jumlah</p>
                            </div>
                            <div class="col-md-7">
                                <input type="number" class="form-control" id="jumlah" name="jumlah"
                                    value="{{ $brg->jumlah }}">
                                @error('jumlah')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Photo</p>
                            </div>
                            <div class="col-md-7">
                                <input class="form-control" id="image-photo" type="file" onchange="previewImageUpdate()"
                                    name="photo">
                                @error('photo')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Review Photo</p>
                            </div>
                            <div class="col-md-7">
                                <img id="image-preview" src="{{ asset('storage/' . $brg->foto) }}"
                                    style="width: 150px !important;">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function previewImageUpdate() {
            document.getElementById("image-preview").style.display = "block";

            const oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image-photo").files[0]);

            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview").src = oFREvent.target.result;
            };
        };
    </script>
@endsection
