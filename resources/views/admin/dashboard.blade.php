@extends('layout.main')

@section('content')
    <h1 class="my-4">Dashboard</h1>
    <div class="row">
        <div class="col-lg-3 col-md-6 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="w-100 d-flex justify-content-between">
                        <h4 class="mb-0">TK A</h4>
                        <div>
                            <span class="badge bg-primary fs-5">{{ $tkA }} <small
                                    class="fs-6">siswa</small></span>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-primary">
                    <a href="{{ route('murid.tkA') }}" class="text-white text-decoration-none">
                        Selengkapnya&nbsp;<i class="fas fa-arrow-circle-right fs-5"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="w-100 d-flex justify-content-between">
                        <h4 class="mb-0">TK B</h4>
                        <div>
                            <span class="badge bg-primary fs-5">{{ $tkB }} <small
                                    class="fs-6">siswa</small></span>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-primary">
                    <a href="{{ route('murid.tkB') }}" class="text-white text-decoration-none">
                        Selengkapnya&nbsp;<i class="fas fa-arrow-circle-right fs-5"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="w-100 d-flex justify-content-between">
                        <h4 class="mb-0">Barang</h4>
                        <div>
                            <span class="badge bg-success fs-5">{{ $brg }} <small
                                    class="fs-6">siswa</small></span>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-success">
                    <a href="{{ route('data.barang') }}" class="text-white text-decoration-none">
                        Selengkapnya&nbsp;<i class="fas fa-arrow-circle-right fs-5"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="w-100 d-flex justify-content-between">
                        <h4 class="mb-0">Pegawai</h4>
                        <div>
                            <span class="badge bg-warning fs-5">{{ $pgw }} <small
                                    class="fs-6">siswa</small></span>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-warning">
                    <a href="{{ route('data.pegawai') }}" class="text-white text-decoration-none">
                        Selengkapnya&nbsp;<i class="fas fa-arrow-circle-right fs-5"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="ratio ratio-16x9">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.149263128721!2d110.45594021450054!3d-6.9916943704149075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708cc24d9c6207%3A0xf05623c4635a2247!2sTaman%20Kanak-Kanak%20Pgri%2073!5e0!3m2!1sid!2sid!4v1637706463911!5m2!1sid!2sid"
                    style="border:0;" allowfullscreen loading="lazy"></iframe>
            </div>
        </div>
    </div>
@endsection
