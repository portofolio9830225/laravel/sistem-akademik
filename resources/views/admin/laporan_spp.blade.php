@extends('layout.main')

@section('content')
    <div class="flash" data-flash="{{ session('status') }}"></div>
    <h1 class="mt-4 mb-3">Pembayaran SPP</h1>
    <div class="row mb-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Filter Data Pembayaran SPP
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 mb-2">
                            <form action="{{ route('laporan.spp') }}" method="get">
                                <label for="bulan" class="form-label">Bulan :</label>
                                <select class="form-select" id="bulan" name="bulan">
                                    <option value="" selected>-- Pilih Bulan --</option>
                                    <option value="Januari">Januari</option>
                                    <option value="Februari">Februari</option>
                                    <option value="Maret">Maret</option>
                                    <option value="April">April</option>
                                    <option value="Mei">Mei</option>
                                    <option value="Juni">Juni</option>
                                    <option value="Juli">Juli</option>
                                    <option value="Agustus">Agustus</option>
                                    <option value="September">September</option>
                                    <option value="Oktober">Oktober</option>
                                    <option value="November">November</option>
                                    <option value="Desember">Desember</option>
                                </select>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="tahun" class="form-label">Tahun :</label>
                            <select class="form-select" id="tahun" name="tahun">
                                <option value="" selected>-- Pilih Tahun --</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                            </select>
                        </div>
                        <div class="col-md-6 text-md-end">
                            <button type="submit" class="btn btn-primary mb-2"><i class="fas fa-eye"></i>
                                Tampil</button>
                            </form>
                            @if ($siswa != 0)
                                @if (count($spp) == 0)
                                    <form action="{{ route('spp.bayar.tambah') }}" method="post" class="d-inline-block">
                                        @csrf
                                        <input type="hidden" name="bulan" value="{{ $bln }}">
                                        <input type="hidden" name="tahun" value="{{ $thn }}">
                                        <button type="submit" class="btn btn-warning mb-2"><i
                                                class="fas fa-plus"></i>&nbsp;Input</button>
                                    </form>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-warning mb-3">
        Menampilkan Data Pembayaran SPP Bulan : <span class="fw-bold">{{ $bln }}</span> Tahun : <span
            class="fw-bold">{{ $thn }}</span>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped" id="table_id">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Kelas</th>
                        <th>Waktu Pembayaran</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($spp) > 0)
                        @foreach ($spp as $sp)
                            <tr>
                                <th>{{ $loop->iteration }}</th>
                                <td>{{ $sp->siswa->nama }}</td>
                                <td>{{ $sp->siswa->kelas }}</td>
                                <td>
                                    @if ($sp->waktu_pembayaran == null)
                                        <span class="badge bg-danger">Belum Ada</span>
                                    @else
                                        <span class="badge bg-success">{{ $sp->waktu_pembayaran }}</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($sp->keterangan == null)
                                        <span class="badge bg-danger">Belum Bayar</span>
                                    @else
                                        <span class="badge bg-success">Sudah Bayar</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($sp->keterangan == null)
                                        <form action="{{ route('spp.bayar') }}" method="post">
                                            @csrf
                                            @method('put')
                                            <input type="hidden" name="id" value="{{ $sp->id }}">
                                            <input type="hidden" name="bln" value="{{ $bln }}">
                                            <input type="hidden" name="thn" value="{{ $thn }}">
                                            <span class="d-inline-block" id="pop" tabindex="0" data-bs-toggle="popover"
                                                data-bs-trigger="hover focus" data-bs-content="Bayar Sekarang">
                                                <button type="submit" class="btn btn-info btn-sm"><i
                                                        class="fas fa-arrow-circle-right"></i></button>
                                            </span>
                                        </form>
                                    @else
                                        <div class="btn btn-success btn-sm"><i class="fas fa-check-circle"></i></div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <th colspan="5" class="text-center">
                                <span class="badge bg-info">Data tidak ada, silahkan input pembayaran terlebih
                                    dahulu.</span>
                            </th>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>

    <script>
        const pops = document.querySelectorAll('#pop');
        const flash = document.querySelector('.flash').getAttribute('data-flash');

        pops.forEach(pp => {
            new bootstrap.Popover(pp)
        });

        $(document).ready(function() {
            $('#table_id').DataTable();
        });

        if (flash == 'tambah') {
            Swal.fire({
                icon: 'success',
                title: 'Data Laporan SPP',
                text: 'Berhasil Ditambahkan',
            })
        } else if (flash.substr(0, 5) == 'bayar') {
            Swal.fire({
                icon: 'success',
                title: 'SPP ' + flash.substr(5),
                text: 'Berhasil Dibayarkan',
            })
        }
    </script>
@endsection
