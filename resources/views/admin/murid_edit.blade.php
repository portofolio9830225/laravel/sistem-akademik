@extends('layout.main')

@section('content')
    <div class="row mt-4">
        <div class="col-12 bg-white shadow-sm p-3">
            <div class="mb-5">
                <h4 class="fs-3 py-2 px-3 border-bottom border-dark border-3 d-inline">Edit Murid</h4>
            </div>
            <form action="{{ route('murid.update') }}" class="d-inline" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <input type="hidden" name="id" value="{{ $siswa->id }}">
                <input type="hidden" name="gambar_lama" value="{{ $siswa->photo }}">
                <input type="hidden" name="kelas" value="{{ $siswa->kelas }}">
                <div class="row mb-3">
                    <div class="col-md-6 mb-3">
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">NISN</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control @error('nisn') is-invalid @enderror" id="nisn"
                                    placeholder="NISN" name="nisn" value="{{ $siswa->nisn }}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Nama Murid</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control @error('murid') is-invalid @enderror" id="murid"
                                    placeholder="murid" name="murid" value="{{ $siswa->nama }}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Nama Wali Murid</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control @error('wali_murid') is-invalid @enderror"
                                    id="wali_murid" placeholder="wali_murid" name="wali_murid"
                                    value="{{ $siswa->nama_wali }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Tanggal Lahir</p>
                            </div>
                            <div class="col-md-7">
                                <input type="date" class="form-control @error('tgl_lahir') is-invalid @enderror" id="ttl"
                                    placeholder="Tanggal Lahir" name="tgl_lahir" value="{{ $siswa->tanggal_lahir }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Alamat</p>
                            </div>
                            <div class="col-md-7">
                                <textarea class="form-control @error('alamat') is-invalid @enderror"
                                    placeholder="Leave a comment here" id="alamat"
                                    name="alamat">{{ $siswa->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Photo</p>
                            </div>
                            <div class="col-md-7">
                                <input class="form-control form-control-sm" id="image-photo" type="file"
                                    onchange="previewImageUpdate()" name="foto">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Review Photo</p>
                            </div>
                            <div class="col-md-7">
                                <img src="{{ asset('storage/' . $siswa->photo) }}" id="image-preview"
                                    style="width: 150px !important;">
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-warning btn-sm">Update</button>
            </form>
            <form action="{{ route('murid.back') }}" method="post" class="d-inline">
                @csrf
                <input type="hidden" name="id_siswa" value="{{ $siswa->id }}">
                <button type="submit" class="btn btn-secondary btn-sm">Kembali</button>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function previewImageUpdate() {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image-photo").files[0]);

            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview").src = oFREvent.target.result;
            };
        };
    </script>
@endsection
