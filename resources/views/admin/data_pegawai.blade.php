@extends('layout.main')

@section('content')
    <div class="flash" data-flash="{{ session('status') }}"></div>
    <div class="w-100 d-flex justify-content-between mt-4 align-items-center mb-3">
        <h1>Data Pegawai</h1>
        <div class="kanan">
            <a href="{{ route('pegawai.tambah') }}" class="btn btn-sm btn-outline-primary"><i
                    class="fa fa-plus"></i>&nbsp;Tambah</a>
        </div>
    </div>

    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Lama Bekerja</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($pegawai) > 0)
                        @foreach ($pegawai as $pgw)
                            @php
                                $blnPgw = date('m', strtotime($pgw->mulai_kerja));
                                $thnPgw = date('Y', strtotime($pgw->mulai_kerja));
                                
                                $blnNow = date('m', time());
                                $thnNow = date('Y', time());
                                
                                $bulan = $blnNow - $blnPgw;
                                $tahun = $thnNow - $thnPgw;
                            @endphp
                            <tr>
                                <th>{{ $loop->iteration }}</th>
                                <td>
                                    <div class="w-100 d-flex">
                                        <img src="{{ asset('storage/' . $pgw->photo) }}" class="me-2"
                                            style="width: auto;height:50px;">
                                        <div class="right">
                                            <span class="fw-bold">{{ $pgw->nama }}</span> <br>
                                            {{ $pgw->jabatan }}
                                        </div>
                                    </div>
                                </td>
                                <td>{{ $tahun }} Tahun {{ $bulan }} Bulan</td>
                                <td>
                                    <a href="{{ route('pegawai.edit', ['id_pgw' => Crypt::encrypt($pgw->id)]) }}"
                                        class="btn btn-sm btn-warning"><i class="fas fa-edit"></i></a>
                                    <div class="btn btn-danger btn-sm"><i class="fas fa-trash"
                                            onclick="hapusPegawai('{{ $pgw->id }}', '{{ route('pegawai.delete') }}')"></i>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            @if (count($pegawai) == 0)
                <div class="w-100 text-center">
                    Belum Ada Data Pegawai
                </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script>
        const flash = document.querySelector('.flash').getAttribute('data-flash');
        if (flash == 'tambah') {
            Swal.fire({
                icon: 'success',
                title: 'Data Pegawai',
                text: 'Berhasil Ditambahkan',
            })
        } else if (flash == "update") {
            Swal.fire({
                icon: 'success',
                title: 'Data Pegawai',
                text: 'Berhasil Diubah',
            })
        }

        function hapusPegawai(id, url) {
            Swal.fire({
                title: 'Yakin Hapus ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios
                        .delete(url, {
                            data: {
                                id: id
                            }
                        })
                        .then(res => {
                            if (res.data) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Data Pegawai',
                                    text: 'Berhasil Dihapus',
                                    timer: 1000,
                                    showConfirmButton: false,
                                })

                                setTimeout(() => {
                                    document.location.href = '/admin/data-pegawai';
                                }, 1000);
                            }
                        })
                }
            })
        }
    </script>
@endsection
