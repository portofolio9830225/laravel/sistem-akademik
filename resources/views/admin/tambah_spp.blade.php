@extends('layout.main')

@section('content')
    <div class="w-100 d-flex justify-content-between mt-4 align-items-center mb-3">
        <h1>Pembayaran SPP</h1>
        <div class="kanan">
            <a href="{{ route('laporan.spp') }}" class="btn btn-sm btn-outline-info"><i
                    class="fas fa-angle-double-left"></i>&nbsp;Kembali</a>
        </div>
    </div>
    <div class="w-100 mb-3">
        <div class="card">
            <div class="card-header bg-primary text-white">
                Input Data Pembayaran SPP
            </div>
            <div class="card-body">
                <form action="{{ route('tambah.spp.post') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-3 mb-2">
                            <label for="exampleInputEmail1" class="form-label">Bulan :</label>
                            <select class="form-select" id="exampleInputEmail1" name="bulan">
                                <option value="" selected>-- Pilih Bulan --</option>
                                <option value="Januari">Januari</option>
                                <option value="Februari">Februari</option>
                                <option value="Maret">Maret</option>
                                <option value="April">April</option>
                                <option value="Mei">Mei</option>
                                <option value="Juni">Juni</option>
                                <option value="Juli">Juli</option>
                                <option value="Agustus">Agustus</option>
                                <option value="September">September</option>
                                <option value="Oktober">Oktober</option>
                                <option value="November">November</option>
                                <option value="Desember">Desember</option>
                            </select>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="exampleInputEmail1" class="form-label">Tahun :</label>
                            <select class="form-select" id="exampleInputEmail1" name="tahun">
                                <option value="" selected>-- Pilih Tahun --</option>
                                <option value="2021">2021</option>
                            </select>
                        </div>
                        <div class="col-md-6 text-md-end">
                            <button type="submit" class="btn btn-success"><i
                                    class="fas fa-eye"></i>&nbsp;Generate</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="alert alert-warning mb-3">
        Input Data Pembayaran SPP Bulan : <span class="fw-bold">{{ $bln }}</span> Tahun : <span
            class="fw-bold">{{ $thn }}</span>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Kelas</th>
                    <th>Waktu Pembayaran</th>
                    <th>Keterangan</th>
                </tr>
                @if (count($spp) > 0)
                    @foreach ($spp as $sp)
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            <td>{{ $sp->siswa->nama }}</td>
                            <td>{{ $sp->siswa->kelas }}</td>
                            <td>
                                @if ($sp->waktu_pembayaran == null)
                                    <span class="badge bg-danger">Belum Ada</span>
                                @else
                                    <span class="badge bg-success">{{ $sp->waktu_pembayaran }}</span>
                                @endif
                            </td>
                            <td>
                                @if ($sp->keterangan == null)
                                    <span class="badge bg-danger">Belum Bayar</span>
                                @else
                                    <span class="badge bg-success">Sudah Bayar</span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <th colspan="5" class="text-center">
                            <span class="badge bg-info">Data tidak ada, silahkan generate terlebih dahulu.</span>
                        </th>
                    </tr>
                @endif
            </table>
        </div>
    </div>
@endsection
