@extends('layout.main')

@section('content')
    <div class="flash" data-flash="{{ session('status') }}"></div>
    <div class="w-100 d-flex justify-content-between mt-4 align-items-center mb-3">
        <h1>Data Barang</h1>
        <div class="kanan">
            <a href="{{ route('barang.tambah') }}" class="btn btn-sm btn-outline-primary"><i
                    class="fa fa-plus"></i>&nbsp;Tambah</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Kondisi</th>
                        <th scope="col">Jumlah</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($barang) > 0)
                        @php
                            $no = 0;
                        @endphp
                        @foreach ($barang as $brg)
                            <tr>
                                <th>{{ $no++ + $barang->firstItem() }}</th>
                                <td>
                                    <div class="w-100 d-flex">
                                        <img src="{{ asset('storage/' . $brg->foto) }}" class="me-2"
                                            style="width: auto;height:50px;">
                                        <div class="right">
                                            <span class="fw-bold">{{ $brg->nama }}</span> <br>
                                        </div>
                                    </div>
                                </td>
                                <td><span class="badge bg-info">{{ $brg->kondisi }}</span></td>
                                <td>{{ $brg->jumlah }}</td>
                                <td>
                                    <a href="{{ route('barang.edit', ['id' => Crypt::encrypt($brg->id)]) }}"
                                        class="btn btn-sm btn-warning"><i class="fas fa-edit"></i></a>
                                    <div class="btn btn-danger btn-sm"
                                        onclick="hapusBarang('{{ $brg->id }}', '{{ route('barang.delete') }}')"><i
                                            class="fas fa-trash"></i>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            {{ $barang->links() }}
            @if (count($barang) == 0)
                <div class="w-100 text-center">
                    Belum Ada Data Barang
                </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script>
        const flash = document.querySelector('.flash').getAttribute('data-flash');
        if (flash == 'tambah') {
            Swal.fire({
                icon: 'success',
                title: 'Data Barang',
                text: 'Berhasil Ditambahkan',
            })
        } else if (flash == 'update') {
            Swal.fire({
                icon: 'success',
                title: 'Data Barang',
                text: 'Berhasil Diubah',
            })
        }

        function hapusBarang(id, url) {
            Swal.fire({
                title: 'Yakin Hapus ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios
                        .delete(url, {
                            data: {
                                id: id
                            }
                        })
                        .then(res => {
                            if (res.data) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Data Pegawai',
                                    text: 'Berhasil Dihapus',
                                    timer: 1000,
                                    showConfirmButton: false,
                                })

                                setTimeout(() => {
                                    document.location.href = '/admin/data-barang';
                                }, 1000);
                            }
                        })
                }
            })
        }
    </script>
@endsection
