@extends('layout.main')

@section('content')
    <div class="row mt-4">
        <div class="col-12 bg-white shadow-sm p-3">
            <div class="w-100 d-flex justify-content-between align-items-center mb-5">
                <h4 class="fs-3 py-2 px-3 border-bottom border-dark border-3 d-inline">Edit Pegawai</h4>
                <div class="kanan">
                    <a href="{{ route('data.pegawai') }}" class="btn btn-secondary">Kembali</a>
                </div>
            </div>

            <form action="{{ route('pegawai.update') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <input type="hidden" name="id" value="{{ $pgw->id }}">
                <input type="hidden" name="photo_lama" value="{{ $pgw->photo }}">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Nama</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="nama" name="nama" value="{{ $pgw->nama }}">
                                @error('nama')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Masuk Kerja</p>
                            </div>
                            <div class="col-md-7">
                                <input type="date" class="form-control" id="kerja" name="mulai_kerja"
                                    value="{{ $pgw->mulai_kerja }}">
                                @error('mulai_kerja')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Jabatan</p>
                            </div>
                            <div class="col-md-7">
                                <select class="form-select" name="jabatan">
                                    @foreach ($jbtn as $item)
                                        @if ($item == $pgw->jabatan)
                                            <option value="{{ $item }}" selected>{{ $item }}</option>
                                        @else
                                            <option value="{{ $item }}">{{ $item }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('jabatan')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Tempat Lahir</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="tmpt" name="tempat_lahir"
                                    value="{{ $pgw->tempat_lahir }}">
                                @error('tempat_lahir')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Tanggal Lahir</p>
                            </div>
                            <div class="col-md-7">
                                <input type="date" class="form-control" id="tgl" name="tanggal_lahir"
                                    value="{{ $pgw->tanggal_lahir }}">
                                @error('tanggal_lahir')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Alamat Rumah</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="alamat_rumah" name="alamat_rumah"
                                    value="{{ $pgw->alamat_rumah }}">
                                @error('alamat_rumah')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">No HP</p>
                            </div>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="no_hp" name="no_hp"
                                    value="{{ $pgw->no_hp }}">
                                @error('no_hp')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Photo</p>
                            </div>
                            <div class="col-md-7">
                                <input class="form-control" id="image-photo" type="file" onchange="previewImageUpdate()"
                                    name="photo">
                                @error('photo')
                                    <div class="form-text text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-5 mb-2">
                                <p class="mb-0 fs-5 fw-normal">Review Photo</p>
                            </div>
                            <div class="col-md-7">
                                <img src="{{ asset('storage/' . $pgw->photo) }}" id="image-preview"
                                    style="width: 150px !important;">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function previewImageUpdate() {
            document.getElementById("image-preview").style.display = "block";

            const oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image-photo").files[0]);

            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview").src = oFREvent.target.result;
            };
        };
    </script>
@endsection
