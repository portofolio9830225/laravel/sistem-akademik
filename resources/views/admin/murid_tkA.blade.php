@extends('layout.main')

@section('content')
    <div class="flash" data-flash="{{ session('status') }}"></div>
    <div class="w-100 d-flex justify-content-between mt-4 align-items-center mb-3">
        <h1 class="mb-0">Murid TK A</h1>
        <div class="kanan">
            <div class="btn btn-sm btn-outline-primary" data-bs-toggle="modal" data-bs-target="#tambah"><i
                    class="fa fa-plus"></i>&nbsp;Tambah</div>
            <div class="btn-group">
                <button class="btn btn-sm btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-bs-toggle="dropdown" aria-expanded="false">
                    Excel
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <li><a class="dropdown-item" href="{{ route('murid.export') }}"><i
                                class="fas fa-download"></i>&nbsp;Template</a></li>
                    <li><a class="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#excel"><i
                                class="fas fa-file-upload"></i>&nbsp;Import</a></li>
                </ul>
            </div>
            @if (count($murid) > 0)
                <div class="btn btn-sm btn-outline-info" id="pindah" onclick="kelasB('{{ route('murid.naik.kelas') }}')">
                    <i class="fas fa-forward"></i>&nbsp;Pindah Kelas B
                </div>
            @endif
        </div>

        {{-- Import --}}
        <div class="modal fade" id="excel" tabindex="-1" aria-labelledby="excelLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <div class="w-100 text-center text-white">
                            <h5 class="modal-title" id="excelLabel">Import Excel</h5>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('murid.import') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <input class="form-control" type="file" id="formFile" required name="file">
                            </div>
                            <div class="w-100 text-end">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="tambah" tabindex="-1" aria-labelledby="tambahLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mx-auto" id="tambahLabel">TK PGRI 73 - Kelas A</h5>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('murid.tambah') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="kelas" value="TK A">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control @error('nisn') is-invalid @enderror"
                                            id="nisn" placeholder="NISN" name="nisn" value="{{ old('nisn') }}">
                                        <label for="nisn">NISN</label>
                                        @error('nisn')
                                            <div class="form-text text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control @error('murid') is-invalid @enderror"
                                            id="murid" placeholder="Nama Murid" name="murid" value="{{ old('murid') }}">
                                        <label for="murid">Nama Murid</label>
                                        @error('murid')
                                            <div class="form-text text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-floating mb-3">
                                        <input type="text" class="form-control @error('wali_murid') is-invalid @enderror"
                                            id="wali_murid" placeholder="Nama Wali Murid" name="wali_murid"
                                            value="{{ old('wali_murid') }}">
                                        <label for="wali_murid">Nama Wali Murid</label>
                                        @error('wali_murid')
                                            <div class="form-text text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-floating mb-3">
                                        <input type="date" class="form-control @error('tgl_lahir') is-invalid @enderror"
                                            id="ttl" placeholder="Tanggal Lahir" name="tgl_lahir">
                                        <label for="ttl">Tanggal Lahir</label>
                                        @error('tgl_lahir')
                                            <div class="form-text text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating mb-3">
                                        <textarea class="form-control @error('alamat') is-invalid @enderror"
                                            placeholder="Leave a comment here" id="alamat"
                                            name="alamat">{{ old('alamat') }}</textarea>
                                        <label for="alamat">Alamat</label>
                                        @error('alamat')
                                            <div class="form-text text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="image-photo" class="form-label">Photo</label>
                                        <input class="form-control form-control-sm" id="image-photo" type="file"
                                            onchange="previewImageUpdate()" name="foto">
                                        @error('foto')
                                            <div class="form-text text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="mb-3 d-none" id="review-photo">
                                        <label for="formFileSm" class="form-label">Review Photo</label>
                                        <img src="" id="image-preview" width="150px">
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">NISN</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Wali Murid</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($murid) > 0)
                        @foreach ($murid as $mrd)
                            <tr>
                                <th>{{ $mrd->nisn }}</th>
                                <td>
                                    <div class="w-100 d-flex">
                                        <img src="{{ asset('storage/' . $mrd->photo) }}" class="me-2"
                                            style="width: auto;height:50px;">
                                        <div class="right">
                                            {{ $mrd->nama }}
                                        </div>
                                    </div>
                                </td>
                                <td>{{ $mrd->nama_wali }}</td>
                                <td>
                                    <a href="{{ route('murid.edit', ['id' => Crypt::encrypt($mrd->id)]) }}"
                                        class="btn btn-sm btn-warning"><i class="fas fa-edit"></i></a>
                                    <div class="btn btn-danger btn-sm"
                                        onclick="hapusSiswa('{{ $mrd->id }}', '{{ route('murid.delete') }}')"><i
                                            class="fas fa-trash"></i>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            @if (count($murid) == 0)
                <div class="w-100 text-center">
                    Belum Ada Data Murid TK A
                </div>
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script>
        function previewImageUpdate() {
            document.getElementById("image-preview").style.display = "block";
            document.getElementById("review-photo").classList.remove('d-none');
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("image-photo").files[0]);

            oFReader.onload = function(oFREvent) {
                document.getElementById("image-preview").src = oFREvent.target.result;
            };
        };

        const flash = document.querySelector('.flash').getAttribute('data-flash');
        if (flash == 'berhasil') {
            Swal.fire({
                icon: 'success',
                title: 'Data TK A',
                text: 'Berhasil Ditambahkan',
            })
        } else if (flash == 'update') {
            Swal.fire({
                icon: 'success',
                title: 'Data TK A',
                text: 'Berhasil Diubah',
            })
        } else if (flash == 'import') {
            Swal.fire({
                icon: 'success',
                title: 'Import Data TK',
                text: 'Berhasil Ditambahkan',
            })
        }

        function kelasB(url) {
            Swal.fire({
                title: 'Yakin Pindah Kelas B ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Lanjut'
            }).then((result) => {
                if (result.isConfirmed) {
                    fetch(url)
                        .then(res => res.text())
                        .then(data => {
                            if (data) {
                                document.location.href = '/admin/data-murid/tk-b';
                            }
                        })
                }
            })
        }

        function hapusSiswa(id, url) {
            Swal.fire({
                title: 'Yakin Hapus ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios
                        .delete(url, {
                            data: {
                                id: id
                            }
                        })
                        .then(res => {
                            if (res.data) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Data TK A',
                                    text: 'Berhasil Dihapus',
                                    timer: 1000,
                                    showConfirmButton: false,
                                })

                                setTimeout(() => {
                                    document.location.href = '/admin/data-murid/tk-a';
                                }, 1000);
                            }
                        })
                }
            })
        }
    </script>
@endsection
