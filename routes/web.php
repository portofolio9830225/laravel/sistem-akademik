<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard');
});

Route::get('/login', [LoginController::class, 'login'])->name('login')->middleware('guest');
Route::post('login', [LoginController::class, 'authenticate'])->name('auth')->middleware('guest');

Route::get('/home', [AdminController::class, 'dashboard'])->name('dashboard');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout')->middleware('auth');

Route::get('/admin/data-murid/tk-a', [AdminController::class, 'murid_tkA'])->name('murid.tkA');
Route::get('/admin/data-murid/tk-b', [AdminController::class, 'murid_tkB'])->name('murid.tkB');
Route::get('/admin/data-murid/naik-kelas', [AdminController::class, 'murid_naik_kelas'])->name('murid.naik.kelas');
Route::get('/admin/data-murid/export', [AdminController::class, 'murid_export'])->name('murid.export');
Route::get('/admin/data-murid/{id}', [AdminController::class, 'murid_edit'])->name('murid.edit');
Route::post('/admin/data-murid', [AdminController::class, 'murid_back'])->name('murid.back');
Route::post('/admin/data-murid/import', [AdminController::class, 'murid_import'])->name('murid.import');
Route::post('/admin/data-murid/tambah', [AdminController::class, 'murid_tambah'])->name('murid.tambah');
Route::put('/admin/data-murid/update', [AdminController::class, 'murid_update'])->name('murid.update');
Route::delete('/admin/data-murid', [AdminController::class, 'murid_delete'])->name('murid.delete');
Route::delete('/admin/data-murid/all-delete', [AdminController::class, 'murid_all_delete'])->name('murid.all.delete');

Route::get('/admin/data-pegawai', [AdminController::class, 'data_pegawai'])->name('data.pegawai');
Route::get('/admin/data-pegawai/tambah', [AdminController::class, 'pegawai_tambah'])->name('pegawai.tambah');
Route::get('/admin/data-pegawai/edit/{id_pgw}', [AdminController::class, 'pegawai_edit'])->name('pegawai.edit');
Route::post('/admin/data-pegawai', [AdminController::class, 'pegawai_store'])->name('pegawai.store');
Route::put('/admin/data-pegawai/update', [AdminController::class, 'pegawai_update'])->name('pegawai.update');
Route::delete('/admin/data-pegawai', [AdminController::class, 'pegawai_delete'])->name('pegawai.delete');

Route::get('/admin/data-barang', [AdminController::class, 'data_barang'])->name('data.barang');
Route::get('/admin/data-barang/tambah', [AdminController::class, 'barang_tambah'])->name('barang.tambah');
Route::get('/admin/data-barang/edit/{id}', [AdminController::class, 'barang_edit'])->name('barang.edit');
Route::post('/admin/data-barang', [AdminController::class, 'barang_store'])->name('barang.store');
Route::put('/admin/data-barang', [AdminController::class, 'barang_update'])->name('barang.update');
Route::delete('/admin/data-barang', [AdminController::class, 'barang_delete'])->name('barang.delete');

Route::get('/admin/pembayaran-spp', [AdminController::class, 'laporan_spp'])->name('laporan.spp');
Route::post('/admin/pembayaran-spp/tambah-spp', [AdminController::class, 'spp_bayar_tambah'])->name('spp.bayar.tambah');
Route::put('/admin/pembayaran-spp/bayar-spp', [AdminController::class, 'spp_bayar'])->name('spp.bayar');
