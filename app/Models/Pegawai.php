<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    use HasFactory;
    protected $table = "pegawai";
    protected $guarded = ['id'];

    public static function jabatan()
    {
        $data = ['Kepala Sekolah', 'Guru Kelas', 'Operator', 'Penjaga Sekolah'];
        return $data;
    }
}
