<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;
    protected $table = 'barang';
    protected $guarded = ['id'];

    public static function kondisi()
    {
        return ['Masih Layak', 'Tidak Layak'];
    }

    public static function letak()
    {
        return ['Lemari Kayu', 'Lemari Kaca', 'Kamar Mandi', 'Gudang', 'UKS', 'Ruang Kelas', 'Ruang Guru', 'Lain-Lain'];
    }

    public static function latest()
    {
        return Barang::orderBy('id', 'desc')->get();
    }
}
