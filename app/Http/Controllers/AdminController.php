<?php

namespace App\Http\Controllers;

use App\Imports\SiswaImport;
use App\Models\Barang;
use App\Models\Pegawai;
use App\Models\Siswa;
use App\Models\Spp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $data['active'] = 'dashboard';

        $data['tkA'] = Siswa::where('kelas', 'TK A')->count();
        $data['tkB'] = Siswa::where('kelas', 'TK B')->count();
        $data['brg'] = Barang::count();
        $data['pgw'] = Pegawai::count();

        return view('admin.dashboard', $data);
    }

    public function data_pegawai()
    {
        $data['active'] = 'pegawai';
        $data['pegawai'] = Pegawai::all();

        return view('admin.data_pegawai', $data);
    }

    public function data_barang()
    {
        $data['active'] = 'barang';
        $data['barang'] = Barang::orderBy('id', 'desc')->paginate(10);

        return view('admin.data_barang', $data);
    }

    public function murid_tkA()
    {
        $data['active'] = 'murid';
        $data['murid'] = Siswa::where('kelas', 'TK A')->latest()->get();
        return view('admin.murid_tkA', $data);
    }

    public function murid_tkB()
    {
        $data['active'] = 'murid';
        $data['murid'] = Siswa::where('kelas', 'TK B')->latest()->get();
        return view('admin.murid_tkB', $data);
    }

    public function murid_naik_kelas()
    {
        $murid = Siswa::where('kelas', 'TK A')->get();

        foreach ($murid as $mrd) {
            $sw = Siswa::find($mrd->id);
            $sw->kelas = 'TK B';
            $sw->save();
        }

        return true;
    }

    public function murid_edit($id)
    {
        $data['active'] = 'murid';
        $data['siswa'] = Siswa::find(\Crypt::decrypt($id));

        return view('admin.murid_edit', $data);
    }

    public function murid_update()
    {
        $foto = request()->file('foto');

        if ($foto != null) {
            if (request()->gambar_lama != 'siswa/avatar.PNG') {
                Storage::delete(request()->gambar_lama);
            }
            $path = request()->file('foto')->store('siswa');
        } else {
            $path = request()->gambar_lama;
        }

        $siswa = Siswa::find(request()->id);
        $siswa->nama = request()->murid;
        $siswa->nisn = request()->nisn;
        $siswa->alamat = request()->alamat;
        $siswa->tanggal_lahir = request()->tgl_lahir;
        $siswa->nama_wali = request()->wali_murid;
        $siswa->photo = $path;
        $siswa->kelas = request()->kelas;
        $siswa->save();

        if (request()->kelas == 'TK A') {
            return redirect()->route('murid.tkA')->with('status', 'update');
        } else {
            return redirect()->route('murid.tkB')->with('status', 'update');
        }
    }

    public function murid_back()
    {
        // return request();
        $sw = Siswa::find(request()->id_siswa);

        // $kelas = str_replace(' ', '-', \Str::lower($sw->kelas));
        $kelas = str_replace(' ', '-', strtolower($sw->kelas));
        return redirect('/admin/data-murid/' . $kelas);
    }

    public function murid_tambah()
    {
        $msg = [
            'murid.required' => 'Wajib diisi',
            'wali_murid.required' => 'Wajib diisi',
            'nisn.required' => 'Wajib diisi',
            'nisn.integer' => 'NISN berupa angka',
            'nisn.unique' => 'NISN sudah ada',
            'tgl_lahir.required' => 'Wajib diisi',
            'alamat.required' => 'Wajib diisi',
            'foto.required' => 'Wajib diisi',
            'foto.mimes' => 'Extension hanya jpg dan png',
            'foto.max' => 'Ukuran File > 1024 KB'
        ];

        request()->validate([
            'murid' => 'required',
            'wali_murid' => 'required',
            'nisn' => 'required|integer|unique:siswa,nisn',
            'tgl_lahir' => 'required',
            'alamat' => 'required',
            'foto' => 'required|mimes:jpg,png|file|max:1024',
        ], $msg);;

        $path = request()->file('foto')->store('siswa');

        $sw = new Siswa;
        $sw->nama = request()->murid;
        $sw->nisn = request()->nisn;
        $sw->alamat = request()->alamat;
        $sw->tanggal_lahir = request()->tgl_lahir;
        $sw->nama_wali = request()->wali_murid;
        $sw->photo = $path;
        $sw->kelas = request()->kelas;
        $sw->save();

        return redirect()->back()->with('status', 'berhasil');
    }

    public function murid_delete()
    {
        $sw = Siswa::find(request()->id);

        if ($sw->photo != 'siswa/avatar.PNG') {
            Storage::delete($sw->photo);
        }

        $usr = Siswa::with('spp')->where('id', request()->id)->first();

        foreach ($usr->spp as $spp) {
            Spp::destroy($spp->id);
        }

        $sw->delete();

        return true;
    }

    public function murid_all_delete()
    {
        $murid = Siswa::all();

        foreach ($murid as $mrd) {
            $sw = Siswa::find($mrd->id);

            if ($sw->photo != 'siswa/avatar.PNG') {
                Storage::delete($sw->photo);
            }

            $usr = Siswa::with('spp')->where('id', $mrd->id)->first();

            foreach ($usr->spp as $spp) {
                Spp::destroy($spp->id);
            }

            $sw->delete();
        }

        return true;
    }

    public function pegawai_tambah()
    {
        $data['active'] = 'pegawai';
        return view('admin.pegawai_tambah', $data);
    }

    public function pegawai_store()
    {
        $msg = [
            'nama.required' => "Nama wajib diisi",
            'mulai_kerja.required' => "Masuk Kerja wajib diisi",
            'jabatan.required' => "Jabatan wajib diisi",
            'tempat_lahir.required' => "Tempat Lahir wajib diisi",
            'tanggal_lahir.required' => "Tanggal Lahir wajib diisi",
            'alamat_rumah.required' => "Alamat Rumah wajib diisi",
            'no_hp.required' => "No HP wajib diisi",
            'no_hp.numeric' => "No HP berupa angka",
            'photo.required' => "Photo wajib diisi",
            'photo.max' => "Maksimal ukuran < 1 MB",
            'photo.mimes' => "File berupa jpg dan png",
        ];

        $validated = request()->validate([
            'nama' => 'required',
            'mulai_kerja' => 'required',
            'jabatan' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat_rumah' => 'required',
            'no_hp' => 'required|numeric',
            'photo' => 'required|mimes:jpg,png|file|max:1024',
        ], $msg);

        $path = request()->file('photo')->store('pegawai');
        $validated['photo'] = $path;

        Pegawai::create($validated);

        return redirect()->route('data.pegawai')->with('status', 'tambah');
    }

    public function pegawai_edit($id_pgw)
    {
        $data['active'] = 'pegawai';
        $data['pgw'] = Pegawai::find(Crypt::decrypt($id_pgw));
        $data['jbtn'] = Pegawai::jabatan();

        return view('admin.pegawai_edit', $data);
    }

    public function pegawai_update()
    {
        $msg = [
            'nama.required' => "Nama wajib diisi",
            'mulai_kerja.required' => "Masuk Kerja wajib diisi",
            'jabatan.required' => "Jabatan wajib diisi",
            'tempat_lahir.required' => "Tempat Lahir wajib diisi",
            'tanggal_lahir.required' => "Tanggal Lahir wajib diisi",
            'alamat_rumah.required' => "Alamat Rumah wajib diisi",
            'no_hp.required' => "No HP wajib diisi",
            'no_hp.numeric' => "No HP berupa angka",
            'photo.max' => "Maksimal ukuran < 1 MB",
            'photo.mimes' => "File berupa jpg dan png",
        ];

        $validated = request()->validate([
            'nama' => 'required',
            'mulai_kerja' => 'required',
            'jabatan' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'alamat_rumah' => 'required',
            'no_hp' => 'required|numeric',
            'photo' => 'mimes:jpg,png|file|max:1024',
        ], $msg);

        if (request()->hasFile('photo')) {
            Storage::delete(request()->photo_lama);
            $path = request()->file('photo')->store('pegawai');
        } else {
            $path = request()->photo_lama;
        }

        $validated['photo'] = $path;

        Pegawai::where('id', request()->id)
            ->update($validated);

        return redirect()->route('data.pegawai')->with('status', 'update');
    }

    public function pegawai_delete()
    {
        Pegawai::destroy(request()->id);
        return true;
    }

    public function barang_tambah()
    {
        $data['active'] = 'barang';
        $data['kondisi'] = Barang::kondisi();
        $data['letak'] = Barang::letak();
        return view('admin.barang_tambah', $data);
    }

    public function barang_store()
    {
        $msg = [
            'nama.required' => "Nama wajib diisi",
            'letak.required' => "Letak wajib diisi",
            'kondisi.required' => "Kondisi wajib diisi",
            'jumlah.required' => "Jumlah wajib diisi",
            'jumlah.numeric' => "Jumlah berupa angka",
            'photo.required' => "Photo wajib diisi",
            'photo.mimes' => "File berupa jpg dan png",
            'photo.max' => "Ukuran < 1 MB",
        ];

        $validated = request()->validate([
            'nama' => "required",
            'letak' => "required",
            'kondisi' => "required",
            'jumlah' => "required|numeric",
            'photo' => "required|mimes:jpg,png|file|max:1024",
        ], $msg);

        $path = request()->file('photo')->store('barang');

        $validated['foto'] = $path;

        Barang::create($validated);

        return redirect()->route('data.barang')->with('status', 'tambah');
    }

    public function barang_edit($id)
    {
        $data['active'] = 'barang';
        $data['kondisi'] = Barang::kondisi();
        $data['letak'] = Barang::letak();
        $data['brg'] = Barang::find(Crypt::decrypt($id));

        return view('admin.barang_edit', $data);
    }

    public function barang_update()
    {
        $msg = [
            'nama.required' => "Nama wajib diisi",
            'letak.required' => "Letak wajib diisi",
            'kondisi.required' => "Kondisi wajib diisi",
            'jumlah.required' => "Jumlah wajib diisi",
            'jumlah.numeric' => "Jumlah berupa angka",
            'photo.mimes' => "File berupa jpg dan png",
            'photo.max' => "Ukuran < 1 MB",
        ];

        $validated = request()->validate([
            'nama' => "required",
            'letak' => "required",
            'kondisi' => "required",
            'jumlah' => "required|numeric",
            'photo' => "mimes:jpg,png|file|max:1024",
        ], $msg);

        if (request()->hasFile('photo')) {
            Storage::delete(request()->photo_lama);
            $path = request()->file('photo')->store('barang');
        } else {
            $path = request()->photo_lama;
        }

        $validated['foto'] = $path;

        Barang::find(request()->id)->update($validated);

        return redirect()->route('data.barang')->with('status', 'update');
    }

    public function barang_delete()
    {
        Barang::destroy(request()->id);
        return true;
    }

    public function laporan_spp()
    {
        $data['active'] = 'spp';

        $bulan = [1 => "Januari", 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $data['bln'] = $bulan[date('n')];

        $data['thn'] = date('Y');

        $data['spp'] = Spp::with('siswa')->where('bulan', $data['bln'])->where('tahun', $data['thn'])->get();

        if (request()->bulan != null && request()->tahun != null) {
            $data['bln'] = request()->bulan;
            $data['thn'] = request()->tahun;
            $data['spp'] = Spp::with('siswa')->where('bulan', $data['bln'])->where('tahun', $data['thn'])->get();
        } elseif (request()->tahun != null) {
            $data['thn'] = request()->tahun;
            $data['spp'] = Spp::with('siswa')->where('bulan', $data['bln'])->where('tahun', request()->tahun)->get();
        } elseif (request()->bulan != null) {
            $data['bln'] = request()->bulan;
            $data['spp'] = Spp::with('siswa')->where('bulan', request()->bulan)->where('tahun', $data['thn'])->get();
        }

        $data['siswa'] = Siswa::count();
        // return $data['spp'];

        return view('admin.laporan_spp', $data);
    }

    public function spp_bayar()
    {
        $jam = date('H:i:s');
        $day = date('d') . '-' . date('m') . '-' . date('Y');

        // return request();

        $sp = Spp::find(request()->id);
        $sp->keterangan = 'Sudah Bayar';
        $sp->waktu_pembayaran = $day . ' ' . $jam;
        $sp->save();

        session()->flash('bulan', request()->bln);
        session()->flash('tahun', request()->thn);

        return redirect()->route('laporan.spp')->with('status', 'bayar ' . $sp->siswa->nama);
    }

    public function spp_bayar_tambah()
    {
        $siswa = Siswa::all();

        foreach ($siswa as $ss) {
            $sp = new Spp;
            $sp->id_siswa = $ss->id;
            $sp->bulan = request()->bulan;
            $sp->tahun = request()->tahun;
            $sp->save();
        }

        return redirect()->route('laporan.spp')->with('status', 'tambah');
    }

    public function murid_import()
    {
        Excel::import(new SiswaImport, request()->file('file'));
        return redirect()->back()->with('status', 'import');
    }

    public function murid_export()
    {
        return Storage::download('excel/siswa.xlsx');
    }
}
